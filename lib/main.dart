import 'dart:math';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sqlite_test/persist_service.dart';
import 'package:sqlite_test/user.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Simple Sqlite Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DatabaseService dbService = DatabaseService();
  List<User> users = [];

  @override
  void initState() {
    _loadUsersFromDb();
    super.initState();
  }

  _saveUserToDb(map) async {
    final user = User.fromJson(map);
    final int id = await dbService.insertUser(user);
    setState(() {
      users.add(user);
    });
  }

  void _loadUsersFromDb() async {
    await dbService.initDatabase();
    final List<User> list = await dbService.getUsers();
    setState(() {
      users = list;
    });
  }

  _showAddDialog() {
    TextEditingController _nameController = new TextEditingController();
    TextEditingController _familyController = new TextEditingController();
    TextEditingController _ageController = new TextEditingController();
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: const Text("Add User"),
        content: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextField(
                controller: _nameController,
                decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)),
                  labelText: 'Name',
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextField(
                controller: _familyController,
                decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)),
                  labelText: 'Family',
                ),
              ),
              SizedBox(
                height: 5,
              ),
              TextField(
                controller: _ageController,
                decoration: new InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)),
                  labelText: 'Age',
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  WhitelistingTextInputFormatter.digitsOnly
                ], // O
              ),
            ],
          ),
        ),
        actions: [
          new FlatButton(
            child: const Text("Save"),
            onPressed: () async {
              await _saveUserToDb({
                "name": _nameController.text,
                "family": _familyController.text,
                "age": int.parse(_ageController.text)
              });
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showAddDialog,
        child: Icon(Icons.add),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) {
          return Card(
            margin: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text(
                  users[index].name,
                  style: Theme.of(context).textTheme.headline6,
                ),
                Text(users[index].family),
                Text(users[index].age.toString()),
              ],
            ),
          );
        },
        separatorBuilder: (context, index) {
          return SizedBox(
            height: 10,
          );
        },
        itemCount: users.length,
      ),
    );
  }
}
