import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlite_test/user.dart';

final tableUser = "users";

class DatabaseService {
  static final DatabaseService _instance = DatabaseService._internal();
  Future<Database> database;

  factory DatabaseService() {
    return _instance;
  }

  DatabaseService._internal() {
    initDatabase();
  }

  initDatabase() async {
    database = openDatabase(
      join(await getDatabasesPath(), 'users.db'),
      onCreate: (db, version) {
        db.execute(
          '''CREATE TABLE $tableUser(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name STRING,
            family STRING,
            age INTEGER,
            createdTime DATETIME)
          ''',
        );
      },
      version: 1,
    );
  }

  Future<int> insertUser(User user) async {
    Database db = await database;
    int id = await db.insert(tableUser, user.toJson());
    return id;
  }

  Future<User> getUser(int id) async {
    Database db = await database;
    List<Map> datas =
        await db.query(tableUser, where: 'id = ?', whereArgs: [id]);
    if (datas.length > 0) {
      return User.fromJson(datas.first);
    }
    return null;
  }

  Future<List<User>> getUsers() async {
    Database db = await database;
    List<Map> datas = await db.query(tableUser);
    if (datas.length > 0) {
      return List<User>.from(datas.map((user) => User.fromJson(user)));
    }
    return [];
  }
}
