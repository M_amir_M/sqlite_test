
class User {
    User({
        this.name,
        this.family,
        this.age,
    });

    final String name;
    final String family;
    final int age;

    User copyWith({
        String name,
        String family,
        int age,
    }) => 
        User(
            name: name ?? this.name,
            family: family ?? this.family,
            age: age ?? this.age,
        );

    factory User.fromJson(Map<String, dynamic> json) => User(
        name: json["name"] == null ? null : json["name"],
        family: json["family"] == null ? null : json["family"],
        age: json["age"] == null ? null : json["age"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "family": family == null ? null : family,
        "age": age == null ? null : age,
    };
}
